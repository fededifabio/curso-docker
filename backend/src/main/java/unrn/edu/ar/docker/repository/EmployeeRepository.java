package unrn.edu.ar.docker.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import unrn.edu.ar.docker.model.Employee;

import java.util.List;

/**
 * Created by achalise on 2/6/17.
 */

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {

    @Override
    List<Employee> findAllById(Iterable<String> strings);

    Employee findByName(String name);

    List<Employee> findEmployeesByDepartmentId(String departmentId);
}
